﻿using System;
using System.Net.WebSockets;
using Bogus;
using CursoOnline.Dominio.Cursos;
using CursoOnline.DominioTest._Builders;
using CursoOnline.DominioTest._Util;
using Moq;
using Xunit;

namespace CursoOnline.DominioTest.Cursos
{
    public class ArmazenadorDeCursoTest
    {
        private CursoDto _cursoDto;
        private ArmazenadorDeCurso _armazenadorDeCurso;
        private Mock<ICursoRepositorio> _cursoRepositorioMock;

        public ArmazenadorDeCursoTest()
        {
            var fake = new Faker();
            _cursoDto = new CursoDto
            {
                Nome = fake.Random.Words(),
                Descricao = fake.Lorem.Paragraph(),
                CargaHoraria = fake.Random.Double(50,1000),
                PublicoAlvo = "Estudante",
                Valor = fake.Random.Double(500,2000)
            };

            _cursoRepositorioMock = new Mock<ICursoRepositorio>();
            _armazenadorDeCurso=new ArmazenadorDeCurso(_cursoRepositorioMock.Object);

        }
        // Exemplo de Mock - Utilizado para verificar algo
        [Fact]
        public void DeveAdicionarCurso()
        {

            _armazenadorDeCurso.Armazenar(_cursoDto);

            //cursoRepositorioMock.Verify(r => r.Adicionar(It.IsAny<Curso>()));
            _cursoRepositorioMock.Verify(r => r.Adicionar(
                It.Is<Curso>(
                    c=>c.Nome == _cursoDto.Nome && 
                       c.Descricao == _cursoDto.Descricao
                       )
                ));
        }
        
        // Exemplo de Stub - Utilizado um objecto mock para configurar para testar/simular um comportamento
        [Fact]
        public void NaoDeveAdicionarCursoComMesmoNomeDeOutroJaGuardada()
        {
            var cursoJaGuardado = CursoBuilder.Novo().ComNome(_cursoDto.Nome).Build();
            _cursoRepositorioMock.Setup(r => r.ObterPeloNome(_cursoDto.Nome)).Returns(cursoJaGuardado);

            Assert.Throws<ArgumentException>(() => _armazenadorDeCurso.Armazenar(_cursoDto)).ComMensagem("Nome do Curso já existe na base de dados");
        }

        [Fact]
        public void NaoDeveInformarPublicoAlvoInvalido()
        {
            var publicoAlvoInvalido = "Médico";

            _cursoDto.PublicoAlvo = publicoAlvoInvalido;

            Assert.Throws<ArgumentException>(() => _armazenadorDeCurso.Armazenar(_cursoDto)).ComMensagem("Público Alvo Inválido");
        }



    }
}
