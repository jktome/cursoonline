﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace CursoOnline.DominioTest
{
    public class MeuPrimeiroTeste
    {
        [Fact]
        public void Testar()
        {
            var variavel1 = 1;
            var variavel2 = 1;

            variavel2 = variavel1;

            Assert.Equal(variavel1,variavel2);
        }
    }
}
